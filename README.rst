Help on module get_coordinate:

NAME
    get_coordinate

FILE
    /home/lzhang/code/ppp-software-tools/trunk/getcoordinate/get_coordinate.py

DESCRIPTION
    Get coordinate from SOPAC database.
    ======================================
    
    At the website http://sopac.ucsd.edu/sector.shtml, sites coordiantes
    can be derived easily. However, if there are many multiple stations'
    coordinates are required, it is not very convenient to click so many
    time. So you can use this tool to do that.
    
    Dependences::
    
      $ sudo pip install requests
    
    Example usage::
    
      $ python get_coordinate.py --site=ffmj --year=2017 --doy=240
      ffmj    4053455.6953     617729.8687    4869395.8460
    
    If you want to get multiple stations' coordinates, you can put all
    station names to on file and run command like::
    
      $ cat station.list
      $ adis ajac algo alic amc2 areq aruc ascg bamf bogi bogt
      $ python get_coordinate.py --site-file=station.list -year=2017 --doy=240
      adis    4913652.6208    3945922.7985     995383.4718
      bogt    1744398.8811   -6116037.0048     512731.8682
      ajac    4696989.2679     723994.7167    4239678.6928
      bamf   -2420721.4043   -3439856.1222    4778532.7411
      alic   -4052052.6367    4212835.9891   -2545104.7269
      amc2   -1248596.3800   -4819428.2062    3976505.9358
      algo     918129.1742   -4346071.3183    4561977.9050
      areq    1942826.2580   -5804070.3512   -1796894.1437
      bogi    3633815.1197    1397454.3144    5035281.1014
      # Cannot get the ascg's coordinate
      ffmj    4053455.6953     617729.8687    4869395.8460
      # Cannot get the aruc's coordinate

FUNCTIONS
    get_sites_names(args)
        Get sites' names from file or command line.
    
    get_sopac_coordinate(year, doy, site)
        Get the sepcial epoch's coordinate from sopac database.
        
        :param year: Year of epoch
        :type year: int
        :param doy: Day of year
        :type doy: int
        :param site: Station name.
        :type: string
        :return: x, y, z coordinate
        :rtype: (float, float, float)
    
    init_parser()
        Arguments initialization.
    
    main()
        Main function to get coordinate.
    
    set_params(year, doy, site)
        Set parameters via year, doy and site.
        
        :param year: Year of epoch
        :type year: int
        :param doy: Day of year
        :type doy: int
        :param site: Station name.
        :type: string

DATA
    GET_PARA = {'doy': '270', 'op': 'getXYZ', 'out': 'csv', 'site': 'ffmj'...
    SOPAC_URL = 'http://sopac.ucsd.edu/gpseDB/coord'


