"""
    Get coordinate from SOPAC database.
    ======================================

    At the website http://sopac.ucsd.edu/sector.shtml, sites coordiantes
    can be derived easily. However, if there are many stations'
    coordinates are required, it is not very convenient to obtain them.
    So you can use this tool to do that.

    Dependences::

      $ sudo pip install requests

    Example usage::

      $ python get_coordinate.py --site=ffmj --year=2017 --doy=240
      ffmj    4053455.6953     617729.8687    4869395.8460

    If you want to get multiple stations' coordinates, you can save all
    stations' names to one file and run command like::

      $ cat station.list
      $ adis ajac algo alic amc2 areq aruc ascg bamf bogi bogt
      $ python get_coordinate.py --site-file=station.list -year=2017 --doy=240
      adis    4913652.6208    3945922.7985     995383.4718
      bogt    1744398.8811   -6116037.0048     512731.8682
      ajac    4696989.2679     723994.7167    4239678.6928
      bamf   -2420721.4043   -3439856.1222    4778532.7411
      alic   -4052052.6367    4212835.9891   -2545104.7269
      amc2   -1248596.3800   -4819428.2062    3976505.9358
      algo     918129.1742   -4346071.3183    4561977.9050
      areq    1942826.2580   -5804070.3512   -1796894.1437
      bogi    3633815.1197    1397454.3144    5035281.1014
      # Cannot get the ascg's coordinate
      ffmj    4053455.6953     617729.8687    4869395.8460
      # Cannot get the aruc's coordinate

"""
# import sys
import time
import argparse

import requests


SOPAC_URL = "http://sopac.ucsd.edu/gpseDB/coord"
GET_PARA = {
    'op': 'getXYZ',
    'out': 'csv',
    'source': "sopac_ats",
    'year': '2017',
    'doy': '270',
    'site': 'ffmj',
}


def set_params(year, doy, site):
    """Set parameters via year, doy and site.

    :param year: Year of epoch
    :type year: int
    :param doy: Day of year
    :type doy: int
    :param site: Station name.
    :type: string
    """

    GET_PARA['year'] = '%04d' % year
    GET_PARA['doy'] = '%02d' % doy
    GET_PARA['site'] = site.lower()
    return GET_PARA


def get_sopac_coordinate(year, doy, site):
    """Get the sepcial epoch's coordinate from sopac database.

    :param year: Year of epoch
    :type year: int
    :param doy: Day of year
    :type doy: int
    :param site: Station name.
    :type: string
    :return: x, y, z coordinate
    :rtype: (float, float, float)

    """
    my_params = set_params(year, doy, site)
    my_request = requests.get(SOPAC_URL, params=my_params)
    if "Status 400 " in my_request.text:
        raise ValueError("Cannot get the %s's coordinate" % site)
    try:
        idx = my_request.text.index(site)
    except IndexError as exception:
        raise IndexError(str(exception))

    record = my_request.text[idx:]
    items = record.split(',')
    try:
        x_site = float(items[2])
        y_site = float(items[3])
        z_site = float(items[4])
    except TypeError as exception:
        raise TypeError(str(exception))
    except IndexError as exception:
        raise IndexError(str(exception) + str(items))
    return x_site, y_site, z_site


def init_parser():
    """Arguments initialization.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--site", help="site name")
    parser.add_argument("--year", help="year")
    parser.add_argument("--doy", help="day of year")
    parser.add_argument("--site-file", help="filename of stations list")
    args = parser.parse_args()
    if not args.year:
        parser.print_help()
        raise ValueError("Year cannot be empty!")
    if not args.doy:
        parser.print_help()
        raise ValueError("doy cannot be empty!")
    if not args.site_file and not args.site:
        parser.print_help()
        raise ValueError("You should assign the site name or \
        station list file")
    return args


def get_sites_names(args):
    """Get sites' names from file or command line.
    """
    sites = []
    if args.site:
        sites.append(args.site)
    if args.site_file:
        with open(args.site_file) as f_stream:
            for line in f_stream:
                items = line.strip().split()
                sites = list(set(sites + items))

    return sites


def main():
    """Main function to get coordinate.
    """
    args = init_parser()
    # check_args(args)
    sites = get_sites_names(args)

    for site in sites:
        time.sleep(1)
        try:
            year = int(args.year)
            doy = int(args.year)
        except TypeError as exception:
            raise TypeError("year or doy can not converted to integer.")
        try:
            x_coor, y_coor, z_coor = get_sopac_coordinate(
                year, doy, site)
        except ValueError as exception:
            print "# " + str(exception)
            continue
        outline = "%s %15.4f %15.4f %15.4f" % (
            site, x_coor, y_coor, z_coor)
        print outline


if __name__ == "__main__":
    main()
